function transformedPosition = transform(robotPose, goalPoint)
    % This rotation matrix  transforms the goal point location from
    % the global coordinate system to the robot's local coordinate system
    %       | cos(theta)    sin(theta) |
    %       | -sin(theta)   cos(theta) |
    % Assumption is that the robot heading is zero when its angle with the 
    % X axis is zero
    
    theta = robotPose(3);
    cTheta = cos(theta);
    sTheta = sin(theta);
    
    newPoint = goalPoint' - robotPose(1:2);
    transformedPosition = [cTheta, sTheta; -sTheta,  cTheta] * newPoint;
end