function projectionPoint = findProjection(currentPose, previousWayPoint, nextWayPoint)
    
    a = currentPose(1:2) - previousWayPoint';
    b = nextWayPoint' - previousWayPoint';
    c = dot(a,b)/norm(b)^2*b;
    
    projectionPoint = c + previousWayPoint';

end

