function [wayPoints, numberOfWaypoints] = generateWayPoints(mainWayPoints, segmentLength)

    numberOfWayPoints = size(mainWayPoints, 1);
    wayPoints = zeros(500, 2);
    
    numberOfWaypoints = 0;
    
    for i = 1:numberOfWayPoints-1
        mainVector = [mainWayPoints(i+1, 1) - mainWayPoints(i, 1), mainWayPoints(i+1, 2) - mainWayPoints(i, 2)];
        baseVector = mainVector/norm(mainVector);
        numberOfSegments = floor(norm(mainVector)/segmentLength);
        
        for k = 1:numberOfSegments
            numberOfWaypoints = numberOfWaypoints + 1;
            aWayPoint = (k * segmentLength * baseVector) + mainWayPoints(i, :);
            wayPoints(numberOfWaypoints, :) = aWayPoint;
        end
        
    end

end

