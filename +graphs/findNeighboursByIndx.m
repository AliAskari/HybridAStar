function [successors, cost] = findNeighboursByIndx(nodeIndx, aGrid, workSpace, varargin)

    % Default connectivity is 4
    if nargin < 4
        connectivity = 4;
    elseif nargin == 4
        connectivity = varargin{1};
    end
    
    if connectivity == 4
	    delta = [        	+1 ;         % up
	                  aGrid.dim ;        % right
	                        -1 ;         % down
	                 -aGrid.dim];        % left
        
        cost = ones(connectivity, 1) * aGrid.RESOLUTION;
             
    elseif connectivity == 8
        delta = [           +1 ;		% up
                   +1+aGrid.dim ;    	 % up-right
                      aGrid.dim ;    	 % right
                   -1+aGrid.dim ;    	 % down-right
                   			-1 ;		% down
                   -1-aGrid.dim ;    	 % down-left
                   	 -aGrid.dim ;    	 % left
                   +1-aGrid.dim];    	 % up-left

        cost = ones(connectivity, 1) * aGrid.RESOLUTION;
        cost(2:2:end) = aGrid.RESOLUTION * sqrt(2);

    else
        error('Invalid connectivity argument. Connectivity is either 4 or 8');
    end
    
    neighbours = delta + double(nodeIndx);

    rangeMask = neighbours > 0 & neighbours <= aGrid.nNodes;
    
    %% remove neighbours not in range
    neighbours = neighbours(rangeMask);
    neighbours(neighbours == 0) = [];
    
    workSpaceMask = (mod(neighbours, aGrid.dim) ~= 1) & ~(workSpace(neighbours) == 1);
    successors = neighbours(workSpaceMask);
    cost = cost(workSpaceMask);
    
    cost(successors == 0) = [];
    successors(successors == 0) = [];

end