function workSpace = createWorkSpace(aGrid)
%#codegen
    workSpace = zeros(aGrid.NUMBER_OF_ROWS, aGrid.NUMBER_OF_COLS);
    
    %% walls around the workspace in workspace to constraint the search space
    workSpace(aGrid.NUMBER_OF_ROWS, :) = 1;
    workSpace(:, aGrid.NUMBER_OF_COLS) = 1;
       
    %% start and goal points
    startIndx = graphs.convertXY2Indx(aGrid, aGrid.startNode);
    goalIndx  = graphs.convertXY2Indx(aGrid, aGrid.goalNode);
    workSpace(startIndx) = 2;
    workSpace(goalIndx) = 3;
    
end