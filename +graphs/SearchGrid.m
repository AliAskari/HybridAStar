classdef SearchGrid < handle
 %#codegen
    %SEARCHGRID Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant = true)
        LIMITS_X        = [-4, 4];
        LIMITS_Y        = [-4, 4];
        RESOLUTION      = 0.05;
        NUMBER_OF_COLS  = ((graphs.SearchGrid.LIMITS_X(2)-graphs.SearchGrid.LIMITS_X(1)) / graphs.SearchGrid.RESOLUTION) + 1;
        NUMBER_OF_ROWS  = ((graphs.SearchGrid.LIMITS_Y(2)-graphs.SearchGrid.LIMITS_Y(1)) / graphs.SearchGrid.RESOLUTION) + 1;
        NUMBER_OF_NODES = graphs.SearchGrid.NUMBER_OF_COLS * graphs.SearchGrid.NUMBER_OF_ROWS;
    end

    properties
        limitsX;
        limitsY;
        nodes; 
		gridX; 
		gridY; 
		nNodes;
		dim;   
		startNodeIndx;
        startNode;
        startState;
		goalNodeIndx;
        goalNode;
        goalState;
        resolution;
        cost;
        heuristic;
    end
    
    methods
        
        % Allocate all the memory that you can/should right here!
        function obj = SearchGrid(startState, goalState)
            
            obj.startState = startState;
            obj.goalState = goalState;
            
            obj.startNode = startState(1:2);
            obj.goalNode = goalState(1:2);
            
        	obj.limitsX = obj.LIMITS_X;
        	obj.limitsY = obj.LIMITS_Y;
            obj.resolution = obj.RESOLUTION;

            nmbrOfPointsX = obj.NUMBER_OF_COLS;
            nmbrOfPointsY = obj.NUMBER_OF_ROWS;

            linX = linspace(obj.LIMITS_X(1), obj.LIMITS_X(2), nmbrOfPointsX);
            linY = linspace(obj.LIMITS_Y(1), obj.LIMITS_Y(2), nmbrOfPointsY);   

            [gridX, gridY] = meshgrid(linX, linY);

            nodesX = gridX(:);
            nodesY = gridY(:);

            % to get around floaing-point erros all numbers are rounded upto 3 decimal palces
            fuckThisShit = 1000;
            nodes = floor(fuckThisShit*[nodesX, nodesY])./fuckThisShit;

            obj.nodes = inf(obj.NUMBER_OF_NODES, 2);
            for i=1:obj.NUMBER_OF_NODES
                obj.nodes(i, 1) = nodes(i, 1); 
                obj.nodes(i, 2) = nodes(i, 2); 
            end
            
            obj.gridX = inf(obj.NUMBER_OF_ROWS, obj.NUMBER_OF_COLS);
            obj.gridY = inf(obj.NUMBER_OF_ROWS, obj.NUMBER_OF_COLS);
            for row = 1:obj.NUMBER_OF_ROWS
                for col = 1:obj.NUMBER_OF_COLS
                    obj.gridX(row, col) = gridX(row, col); 
                    obj.gridY(row, col) = gridY(row, col); 
                end
            end
            
            obj.nNodes = obj.NUMBER_OF_NODES;
            obj.dim    = sqrt(obj.nNodes);

            % Start and Goal nodes might have some offsett with regards to the
            % points in the grid so remove that offset to ensure they are indeed
            % a cell in the matrix
            obj.startNodeIndx = graphs.convertXY2Indx(obj, obj.startNode);
            obj.goalNodeIndx  = graphs.convertXY2Indx(obj, obj.goalNode);
            
            % Set the cost for all nodes to infinity!
            obj.cost = inf(obj.NUMBER_OF_NODES, 1);
            
            % Set the cost for all nodes to infinity!
            obj.heuristic = inf(obj.NUMBER_OF_NODES, 1);
            
        end

    end
    
end

