% This function converts [X, Y] on a grid created by this command:
% meshgrid(-gridLim:res:gridLim) to the linear index of that point

function [indx, varargout] = convertXY2Indx(aGrid, XY)    

    x = XY(1);
    y = XY(2);
    
    if (x < aGrid.limitsX(1)) || (x > aGrid.limitsX(2))
        indx = -1;
        return;
        %error('x ( = %f ) is not in the limit: [%f, %f]', x, aGrid.limitsX(1), aGrid.limitsX(2));
    elseif (y < aGrid.limitsY(1)) || (y > aGrid.limitsY(2))
        indx = -1;
        return;
        %error('y ( = %f ) is not in the limit: [%f, %f]', y, aGrid.limitsY(1), aGrid.limitsY(2));
    end
          
    rangeX = aGrid.limitsX(2) - aGrid.limitsX(1);
    rangeMinX = aGrid.limitsX(1);   
    rangeY = aGrid.limitsY(2) - aGrid.limitsY(1);
    rangeMinY = aGrid.limitsY(1);
    
    %% to get around floaing-point erros all numbers are rounded upto 3 decimal palces
%     fuckThisShit = 1000;  
%     x = fuckThisShit*floor(x/aGrid.resolution)*aGrid.resolution/fuckThisShit;
%     if x < aGrid.limitsX(1)
%         x = aGrid.limitsX(1);
%     elseif x > aGrid.limitsX(2)
%         x = aGrid.limitsX(2);
%     end
%     
%     y = fuckThisShit*floor(y/aGrid.resolution)*aGrid.resolution/fuckThisShit;
%     if y < aGrid.limitsY(1)
%         y = aGrid.limitsY(1);
%     elseif y > aGrid.limitsY(2)
%         y = aGrid.limitsY(2);
%     end
    
    %% Find the right index using simple math!
  
    % Find how many elements there are in each column (i.e. Number of Rows)
    nmbrOfElemntsPerColumn = floor(rangeY/aGrid.resolution) + 1;    
    
    % Find how many columns there are between current point and the first
    % column from left on the grid
    nthCol = floor((abs(x-rangeMinX)/aGrid.resolution));
    
    % Calculate the total number of elements in columns up to the current one
    % if the node lies on the first column there exist no previous column    
    indxX = (nthCol) * nmbrOfElemntsPerColumn;
    
%     if (nthCol ~= 0)
%         indxX = (nthCol) * nmbrOfElemntsPerColumn;
%     elseif (nthCol == 0)
%         indxX = 0;
%     end       
    
    % Calculate the number of elements from the begenning of the current
    % column up to the current point
    indxY = floor((abs(y-rangeMinY)/aGrid.resolution)) + 1;
    
    indx = (indxX + indxY);     
        
%     minimumDistance = sqrt((obj.nodes(:, 1) - x).^2 + (obj.nodes(:, 2) - y).^2);  
%     [~, indx2] = min(minimumDistance);  
%     if indx2 ~= indx
%        % error('Wrong calculation');
%     end
    %% return the modified [x, y] if required!
    
    if nargout == 2
        varargout{1} = [x, y];
    end
    
end