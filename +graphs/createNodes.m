function allNodes = createNodes(rangeX, rangeY, resolution)

    nSectionsX = (rangeX(2) - rangeX(1))/resolution;
    nSectionsY = (rangeY(2) - rangeY(1))/resolution;
    X = linspace(rangeX(1), rangeX(2), nSectionsX + 1);
    Y = linspace(rangeY(1), rangeY(2), nSectionsY + 1);
    
    allNodes = [X', Y'];
    
end

