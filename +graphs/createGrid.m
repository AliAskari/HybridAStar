function aGrid = createGrid(aGrid)

    nmbrOfPointsX = (aGrid.limitsX(2) - aGrid.limitsX(1))/aGrid.resolution + 1;
    nmbrOfPointsY = (aGrid.limitsY(2) - aGrid.limitsY(1))/aGrid.resolution + 1;
    linX = linspace(aGrid.limitsX(1), aGrid.limitsX(2), nmbrOfPointsX);
    linY = linspace(aGrid.limitsY(1), aGrid.limitsY(2), nmbrOfPointsY);    
    [gridX, gridY] = meshgrid(linX, linY);

    nodesX = gridX(:);
    nodesY = gridY(:);
    
    % to get around floaing-point erros all numbers are rounded upto 3 decimal palces
    fuckThisShit = 1000;
    nodes = floor(fuckThisShit*[nodesX, nodesY])./fuckThisShit;
    
    aGrid.nodes  = nodes;   
    aGrid.gridX  = gridX;
    aGrid.gridY  = gridY;
    aGrid.nNodes = size(nodes, 1);
    aGrid.dim    = sqrt(aGrid.nNodes);
    
    % Start and Goal nodes might have some offsett with regards to the
    % points in the grid so remove that offset to ensure they are indeed
    % a cell in the matrix
    aGrid.startNodeIndx = graph.convertXY2Indx(aGrid, aGrid.startNode);
    aGrid.goalNodeIndx  = graph.convertXY2Indx(aGrid, aGrid.goalNode);
    
end