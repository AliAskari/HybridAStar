function vertices = rectangularObstacles(obstacle)

% each row of obstacles contain the following information about an
% individual obstacle, it is assumed all obstacles are rectangular
% obstacles(1, :) = obstaclePosX
% obstacles(2, :) = obstaclePosY
% obstacles(3, :) = obstaclePosTheta
% obstacles(4, :) = obstacleLength
% obstacles(5, :) = obstacleWidth

    x = obstacle(1);
    y = obstacle(2);
    theta = obstacle(3); 
    L = obstacle(4); 
    W = obstacle(5);
    
    cTheta = cos(theta);
    sTheta = sin(theta);
    transformationMatrix = [cTheta, sTheta; ...
                           -sTheta, cTheta];

%     vertices2 = [-L/2, -W/2; ...
%                  L/2, -W/2; ...
%                  L/2,  W/2; ...
%                 -L/2,  W/2];

    % Instead of using closePolygonParts add the initial vertice as the
    % last vertice and "Close" the polygon! To reduce the number of elemnts
    % an if condition could be added to check the first and last rows and
    % if they are equal it is not necessary to addd the first element to
    % the list as the last element!
    
    vertices = [-L/2, -W/2; ...
                 L/2, -W/2; ...
                 L/2,  W/2; ...
                -L/2,  W/2; ...
                -L/2, -W/2];

    vertices = bsxfun(@plus, vertices*transformationMatrix, [x, y]);
%     vertices2 = bsxfun(@plus, vertices2*transformationMatrix, [x, y]);
%     [xv, yv] = closePolygonParts(vertices(:, 1), vertices(:, 2));
%     vertices = [xv, yv];   
%     isEqual = isequal(vertices, vertices2);  
%     if (isEqual == 0)
%        error('Humm, You''ve Fucked up somewhere!');
%     end
    
end