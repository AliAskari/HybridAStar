function [allVertices, workSpace] = loadObstacles(grid, workSpace, obstacleArray, realObstacles)

    % defaultObstacleDimensions = [L, W];
    % TODO: This should also be loaded at init
%     defaultObstacleDimensions = [1.5, 0.75];
    nObstacles = size(obstacleArray, 1);
    obstacles = zeros(nObstacles, 5);
    
    if (realObstacles == true)
        obstacleDimensions = visual.loadObstacleDimensions();
    else
        obstacleDimensions = obstacleArray(:, 4:5);
    end
        

    for i=1:nObstacles
        obstacles(i, 1) = obstacleArray(i, 1);
        obstacles(i, 2) = obstacleArray(i, 2);
        obstacles(i, 3) = obstacleArray(i, 3);
        obstacles(i, 4) = obstacleDimensions(i, 1);
        obstacles(i, 5) = obstacleDimensions(i, 2);         
    end
      
    allVertices = cell(nObstacles, 1);
  
    for i = 1:nObstacles
        vertices =  graphs.rectangularObstacles(obstacles(i, :));            
       
        n = size(vertices, 1);
        edges  = [(1:n-1)', (2:n)'; 
                        n ,     1];
        in = graphs.inpoly([grid.gridX(:), grid.gridY(:)], vertices, edges);

        workSpace(in) = 1;
        allVertices{i} = vertices;
    end      
    
end

