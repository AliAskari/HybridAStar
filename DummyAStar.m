function AStarpathLength = DummyAStar(currentChild, aGridNodes, workSpace, verticesList, goalState)
    
    % Run A* and find the real distance to the goal
    % (1) Create a temporary grid
    tempStartNode = aGridNodes(currentChild, 1:2);
    tempGrid = graphs.SearchGrid(tempStartNode, goalState(1:2));
    % (2) Run the A* search
    [~, cameFrom, goalFoundAStar] = search.aStar(tempGrid, workSpace, verticesList);
    % (3) Find the length of the path
    if goalFoundAStar == true
        AStarpathLength = search.findPathNormalAStar(tempGrid, cameFrom);
    else
        AStarpathLength = 0;
    end
            
end