function [v,w] = diff2Uni(uR, uL)
    %wheel radius and distance between the wheels
    R = 0.0765;
    L = 0.40;
    
    v = R/2*(uR+uL);
    w = R/L*(uR-uL);
end