function [uR, uL] = uni2Diff(v, w)
    %wheel radius and distance between the wheels
    R = 0.0765;
    L = 0.40;
    
    uR = (2*v+w*L)/(2*R);
    uL = (2*v-w*L)/(2*R);
end