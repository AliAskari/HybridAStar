function indx = find(Matrix, vector)
%FIND Searches through rows of matrix for vecotr and returns its index
    [~, indx] = ismember([vector(1), vector(2)], Matrix, 'rows');
end

