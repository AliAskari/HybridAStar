function [repulsivePotential, distancePoly] = calculatePotentialFunction(aGrid, verticesList)

    q = aGrid.nodes;

    %% set up obstacles and repulsive potential and force field
    nObstacles = size(verticesList, 1);
    
    repulsivePotential = zeros(aGrid.nNodes, 1);
    thresholdDistance = 0.2;
    distancePoly = zeros(aGrid.nNodes, 1);
    
    maxPotential = 10;
    
    for j = 1:nObstacles
        v = verticesList{j};
        for i = 1:aGrid.nNodes
            [d, ~, ~] = potential.p_poly_dist(q(i,1), q(i,2), v(:, 1), v(:, 2));
            distancePoly(i) = d;
            if ((d <= thresholdDistance) && (d > 0))
                repulsivePotential(i) = 1/2*(1/d - 1/thresholdDistance)^2+ repulsivePotential(i);
%                 repulsivePotential(i) = abs(1/d - 1/thresholdDistance);% + repulsivePotential(i);
            elseif ((d == thresholdDistance) || (d <= 0))
                repulsivePotential(i) = maxPotential;
%                 repulsivePotential(i) = 2*abs(1/d - 1/thresholdDistance);
            end
        end
    end

    %repulsivePotential(repulsivePotential>maxPotential) = maxPotential;

%     repulsivePotential = repulsivePotential/maxPotential;
%     repulsivePotential = reshape(repulsivePotential, gridDim);
    
    for i = 1:aGrid.nNodes
        if repulsivePotential(i) > maxPotential
            repulsivePotential(i)  = maxPotential;
        end
    end
    
end