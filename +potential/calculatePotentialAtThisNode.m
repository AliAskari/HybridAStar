function repulsivePotential = calculatePotentialAtThisNode(thisNodeIndex, aGrid, verticesList)

    % find the node using its index
    thisNode = aGrid.nodes(thisNodeIndex, :);
    
    % set up obstacles and repulsive potential and force field
    nObstacles = size(verticesList, 1);
    repulsivePotential = 0;
    
    % TODO: thresholdDistance should be porportionate to the size of the
    % moving search agent!
    thresholdDistance = 0.2;
    
    % maximum potential should be related to the gird resolution and the
    % moevement cost between nodes.
    maxPotential = 10;
    
    for j = 1:nObstacles
        v = verticesList{j};
        [d, ~, ~] = potential.p_poly_dist(thisNode(1), thisNode(2), v(:, 1), v(:, 2));       
        if ((d <= thresholdDistance) && (d > 0))
            repulsivePotential = 1/2*(1/d - 1/thresholdDistance)^2 + repulsivePotential;
        elseif ((d == thresholdDistance) || (d <= 0))
            repulsivePotential = maxPotential;
        end
    end

    if (repulsivePotential > maxPotential)
        repulsivePotential = maxPotential;
    end

end