function target = reynolds(vD, currentPose, previousWayPoint, nextWayPoint)

    % predict future position of the robot assumin no change in heading
    [uR, uL] = kinematics.uni2Diff(vD, 0);
    theta = currentPose(3);
    %wheel radius and distance between the wheels
    R = 0.0765;
    vX = R * (uR+uL) * cos(theta) * 0.5;
    vY = R * (uR+uL) * sin(theta) * 0.5;
    normalizedVelocity = [vX, vY]./norm([vX, vY]);
    
    dt = [0.01; 0.01];
    predictedPose = currentPose(1:2) + normalizedVelocity*dt;
    a = predictedPose - previousWayPoint';
    b = nextWayPoint' - previousWayPoint';
%     angle = atan2(norm(cross(a,b)),dot(a,b));
    c = dot(a,b)/norm(b)^2*b;
    target = c*1.05;
    
    projectionPoint = c + previousWayPoint';
    target = previousWayPoint + target';
    

end