function curvature = purePursuit(goalPoint)

    if (norm(goalPoint) > 0.5)
        lookForward = 0.5;
    else
        lookForward = norm(goalPoint);
    end
    curvature = 2*goalPoint(2)/(lookForward^2);

end