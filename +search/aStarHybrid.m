function [costSoFar, closedSet, cameFrom, searchAgentState, dubinsNodes, dubinsCollision, numberOfDubinsNodes, lastExpandedNodeIndx] = aStarHybrid(aGrid, workSpace, verticesList)
%#codegen

    plotCounter = 0;
    % Should use Dubins path/heuristic
    isDubinsHeuristic = true;
    
    % lastExpandedNode
    lastExpandedNodeIndx = nan;
    
    % Should try to find the goal using dubins path
    dubinTryCounter = 0;
    
    dubinsNodes = nan(dubins.CONSTANTS.MAXNODESINPATH, 3);
    numberOfDubinsNodes = 0;
    dubinsCollision = true;
    
    % For each node, which node it can most efficiently be reached from
    % If a node can be reached from many nodes, cameFrom will eventually
    % contain the most efficient previous step
    cameFrom                      = zeros(aGrid.NUMBER_OF_NODES, 1);
    cameFrom(aGrid.startNodeIndx) = aGrid.startNodeIndx;

    % The set of nodes already evaluated
    closedSet = zeros(aGrid.NUMBER_OF_NODES, 1);

    % The set of currently discovered nodes still to be evaluated
    openSet = zeros(aGrid.NUMBER_OF_NODES, 1);
    
    % The minimum cost to each node up to now
    costSoFar = zeros(aGrid.NUMBER_OF_NODES, 1);
    
    % f = g (cost so far) + h (heuristic)
    % All nodes have infinite f values at the begenning
    f = inf(aGrid.NUMBER_OF_NODES, 1);

    % Start node is the only known node initially
    openSet(aGrid.startNodeIndx) = 1;    
    % The cost to start node is obviously zero
    costSoFar(aGrid.startNodeIndx) = 0;
    % The fScore value for the start node is just the heuristic value
    f(aGrid.startNodeIndx) = 0;

    % An array to keep track of the state of search agent!
    searchAgentState = nan(aGrid.NUMBER_OF_NODES, 3);
    searchAgentState(aGrid.startNodeIndx, :) = aGrid.startState;
   
    while sum(openSet) ~= 0
        % Find the index haveing minimum fScore value
        [~, minFIndx] = min(f);       
        % Make sure this node is not picked up again by setting it's fScore
        % to max
        f(minFIndx) = inf;
        currentNodeIndx = minFIndx;

        % Get the search agent state
        currentSearchAgentState = searchAgentState(minFIndx, :);
      
        if currentNodeIndx == aGrid.goalNodeIndx
            dubinsCollision = true;
%             disp('goal Found');
            lastExpandedNodeIndx = currentNodeIndx;
            return
        end
        
        if (isDubinsHeuristic == true)
            
            dubinTryCounter = dubinTryCounter + 1;
            % Try to see if we can reach a clean path to goal an a Dubins path
            if dubinTryCounter > 10
                % Reset the counter
                dubinTryCounter = 0;

                minTurnRadius = 0.2;
                % Calculate dist to goal
                thisNode = searchAgentState(currentNodeIndx, :);
                dist = norm(thisNode(1:2) - aGrid.goalState(1:2));
                % This is an expensive operation so do it onlt if we are close
                % enough to the goal!
                if dist < dubins.CONSTANTS.TRIGGERDISTANCE
                    [dubinsNodes, numberOfDubinsNodes, pathLength, collision] = dubins.Shoot(workSpace, aGrid, thisNode, aGrid.goalState, verticesList, minTurnRadius);
                    % Only continue with dubins path if the dubins path lenght is shorter than 1.5 m
                    if (pathLength < 1.5)
	                    % If there is no collision we are good to go. Yaay!
	                    if collision == false
	                        dubinsCollision = false;
	                        lastExpandedNodeIndx = currentNodeIndx;
                            plot(dubinsNodes(1:numberOfDubinsNodes, 1), dubinsNodes(1:numberOfDubinsNodes, 2), ...
                            'Color', constants.Colors.LIGHTGRAY);

%                             drawnow;
%                             figName = strcat('HybridAStar_' , num2str(globalIterCounter), '/frame_', num2str(plotCounter));
%                             export_fig(figName, '-m2.5', '-png');
%                             pause(0.1);
	%                         disp('goal Found');
	                        return;
	                    end
                    end
                end
            end

        end


        plotCounter = plotCounter + 1;
        
%         if (mod(plotCounter, 5) == 0)
%             drawnow;
%             figName = strcat('HybridAStar_' , num2str(globalIterCounter), '/frame_', num2str(plotCounter));
%             export_fig(figName, '-m2.5', '-png');
%             pause(0.1);
%         end
        
        [childNodes, childStates, cost, notValid] = search.driveToNeighbours(currentSearchAgentState, aGrid, workSpace);

%         workSpace(currentNodeIndx) = 4;
%         workSpace = search.plotNeighbours(aGrid.gridX, aGrid.gridY, workSpace, childNodes, childStates);
%         pause 
        
        tempCostSoFar = costSoFar(currentNodeIndx);
        prevChild = nan;
        
        for i = 1:numel(childNodes)
            
            currentChild = childNodes(i, :);

            if notValid(i) == true
                continue;
            end
            
            if closedSet(currentChild) == 1
                continue
            end
            
            % tentativeGScore = 
            % (1) Cost up to this node 
            % (2) The cost to move from the orevious node to this node
            % (3) The cost generated by potential function on the next node
            tentativeGScore = tempCostSoFar + ...
                              cost(i) + ...
                              potential.calculatePotentialAtThisNode(currentChild, aGrid, verticesList);                              
%                               aGrid.cost(currentChild);

        
            % Add the node to the list of nodes to be evaluated
            if openSet(currentChild) == 0
                % Add the current child node to the open list of nodes to
                % be evaluated/expanded
                openSet(currentChild) = 1; 
            elseif tentativeGScore >= costSoFar(currentChild)
                continue
            end

            AStarpathLength = DummyAStar_mex(currentChild, aGrid.nodes, workSpace, verticesList, aGrid.goalState(1:2));

            if (isDubinsHeuristic == true)
                % Here we have to calculate two different heuristics
                % (1) Is the Constrained heuristic which is th length of dubins path
                % time ~= 36.365048
                minTurnRadius  = 0.15;
                aDubinsPath = dubins.DubinsPath();
                aDubinsPath = dubins.Init(childStates(i, :), aGrid.goalState, minTurnRadius, aDubinsPath);                   
                DubinsPathLength  = aDubinsPath.GetPathLength();

                % Final heuristic is the maximum of Constrained Heuristic and Unconstrained Heuristic
                h = max(DubinsPathLength, AStarpathLength);
                tempF = tentativeGScore + h;
            else
                % time ~= 3.6365048
                tempF = tentativeGScore + dist2Goal;               
            end
            

            if (currentChild == prevChild)
                if (tempF > f(currentChild))
                    continue;
                end
            end

            % If we are still on the same node and the cost is lower set
            % the parent to be the parent of the parent
            if (currentChild == currentNodeIndx) 
                cameFrom(currentChild) = cameFrom(currentNodeIndx);
            else
                cameFrom(currentChild) = currentNodeIndx;
            end

            searchAgentState(currentChild, :) = childStates(i, :);
            costSoFar(currentChild) = tentativeGScore;
            f(currentChild) = tempF;

            prevChild = currentChild;
            
        end

        % OK. The current node is expanded
        % (1) Removed it from the set of open nodes
        openSet(currentNodeIndx)   = 0;
        % (2) Add it to the set of closed nodes so that it's not selected
        % any more
        closedSet(currentNodeIndx) = 1;
        
    end
end
