function [pathOfStates, numberOfWayPoints] = findPath(aGrid, cameFrom, searchAgentState, lastExpandedNode)

    current = lastExpandedNode;
    pathOfStates = inf(aGrid.NUMBER_OF_NODES, 3);
    
    numberOfWayPoints = 1;
    pathOfStates(numberOfWayPoints, :) = searchAgentState(current, :);
    
    while current ~= aGrid.startNodeIndx
        numberOfWayPoints = numberOfWayPoints+1;
        current = cameFrom(current);
        pathOfStates(numberOfWayPoints, :) = searchAgentState(current, :);
%         plot(searchAgentState(current, 1), searchAgentState(current, 2), 'r*');
    end

end
