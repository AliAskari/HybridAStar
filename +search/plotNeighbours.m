function workSpace = plotNeighbours(aGrid, workSpace, childNodes, varargin)

    % To make sure the start node and goal node are not covered by a
    % different color we always override their values
    workSpace(aGrid.goalNodeIndx)  = 2;
    workSpace(aGrid.startNodeIndx) = 3;
    
    workSpace(childNodes) = 5;
    pcolor(aGrid.gridX, aGrid.gridY, workSpace);
    
    cmap = [1.00, 1.00, 1.00;	%freeSpace (white) workSpace = 0
            0.00, 0.00, 0.00;	%wallSpace (black) workSpace = 1
            0.92, 0.29, 0.38; 	%startState (red)
            0.30, 0.53, 0.86; 	%goalState (blue)                    
            0.21, 0.73, 0.52;	%path (green)
            0.50, 0.50, 0.50];  %visited
        
    axis square
    colormap(cmap); 

    if (nargin > 3)
        childStates = varargin{1};
        hold on
        plot(childStates(:, 1), childStates(:, 2), 'r*');
        hold off
    end
end