function h = calculateHeuristic(aGrid, heuristicType)

    if strcmp(heuristicType, 'manhattan') == true        
        deltaX = abs(aGrid.nodes(:, 1) - aGrid.goalNode(1));
        deltaY = abs(aGrid.nodes(:, 2) - aGrid.goalNode(2));
        h = deltaX+deltaY;
    elseif strcmp(heuristicType, 'euclidean') == true
        h = sqrt(sum(bsxfun(@minus, aGrid.nodes, aGrid.goalNode).^2, 2)); 
    elseif strcmp(heuristicType, 'octile') == true
        deltaX = abs(aGrid.nodes(:, 1) - aGrid.goalNode(1));
        deltaY = abs(aGrid.nodes(:, 2) - aGrid.goalNode(2));
        h = max(deltaX, deltaY) + (sqrt(2)-1)*min(deltaX, deltaY);
    end
      
    % tie breaking
    gridXRange = aGrid.limitsX(2)-aGrid.limitsX(1);
    D = 1.1; % http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html#heuristics-for-grid-maps
    p = aGrid.resolution/(1.4*(gridXRange));
    h = h*D*(1.0 + p);
    %h = h*(1.0 + 0.1);

end

