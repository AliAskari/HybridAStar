function [visitedNodes, cameFrom] = bfs(grid, workSpace)
%BFS Breath first search algorithm takes allNodes as input
% the input is a vector of allNodes, rootNode in the allNodes. It performs
% bfs algotihm on the allNodes
    plotHandle = 0;

    frontier = tools.CQueue();
    frontier.push(grid.startNodeIndx);
    
    cameFrom(grid.nNodes, 1) = 0;
    cameFrom(grid.startNodeIndx) = grid.startNodeIndx;
    
    % A list of visited nodes for plotting
    visitedNodes(grid.nNodes, 1) = 0;

    while (true)
        if frontier.isempty()
            disp('No solution found.');
            return
        end

        currentNodeIndx = frontier.pop();
        visitedNodes(currentNodeIndx) = 1;   
        
        if currentNodeIndx == grid.goalNodeIndx
            disp('Solution found.');
            return;
        end
        
                 
        neighbours = graph.findNeighboursByIndx(currentNodeIndx, grid, workSpace, 8);
        
        for i = 1:size(neighbours, 1)
            % find index of the current node in the allNodes and check it's
            % value in the distanceVector
             indx = neighbours(i, :);           
            if (cameFrom(indx) == 0)                        
                frontier.push(indx);        
                cameFrom(indx) = currentNodeIndx;
                
%                 if plotHandle == 0
%                     [workSpace, plotHandle] = visual.plotNode(indx, grid, workSpace);
%                 else
%                     [workSpace, plotHandle] = visual.plotNode(indx, grid, workSpace, plotHandle);
%                 end
%                 pause;
            end
        end
    end   
end