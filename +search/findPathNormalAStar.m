function AStarpathLength = findPathNormalAStar(aGrid, cameFrom)

    current = aGrid.goalNodeIndx;
    AStarpathLength = 0;

    while current ~= aGrid.startNodeIndx
        prev = current;
        current = cameFrom(current);
        AStarpathLength = AStarpathLength + norm(aGrid.nodes(current, :) - aGrid.nodes(prev, :));
    end

end