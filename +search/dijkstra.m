function [visitedNodes, cameFrom] = dijkstra(grid, workSpace)
% Dijkstra search algorithm takes allNodes as input
% the input is a vector of allNodes
    plotHandle = 0;

    frontier = tools.PriorityQueue();
    frontier.push(grid.startNodeIndx, 0);
    
    cameFrom(grid.nNodes, 1) = 0;
    cameFrom(grid.startNodeIndx) = grid.startNodeIndx;

    costSoFar(grid.nNodes, 1) = 0;    
    
    % A list of visited nodes for plotting
    visitedNodes(grid.nNodes, 1) = 0;
    
    while (true)
        if frontier.isEmpty()
            disp('No solution found.');
            return
        end

        currentNodeIndx = frontier.pop();
        if currentNodeIndx == grid.goalNodeIndx
            disp('Solution found.');
            return;
        end
           
        visitedNodes(currentNodeIndx) = 1;
        neighbours = graph.findNeighboursByIndx(currentNodeIndx, grid, workSpace, 4);
        
        for i = 1:size(neighbours, 1)
            % find index of the current node in the allNodes and check it's
            % value in the distanceVector
             indx = neighbours(i, :);           
             newCost = costSoFar(indx) + 0.15;
            if (costSoFar(indx) == 0 || newCost < costSoFar(indx))   
                costSoFar(indx) = newCost;
                frontier.push(indx, newCost);        
                cameFrom(indx) = currentNodeIndx;
                
%                 if plotHandle == 0
%                     [workSpace, plotHandle] = visual.plotNode(indx, grid, workSpace);
%                 else
%                     [workSpace, plotHandle] = visual.plotNode(indx, grid, workSpace, plotHandle);
%                 end
%                 pause;
            end
        end
    end   
end