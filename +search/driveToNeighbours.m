function [successors, childStates, cost, notValid] = driveToNeighbours(currentAgentState, aGrid, workSpace)
%#codegen

    % drive the kinematic model of the robot with dfferent w's and get the
    % state of the robot after each 
    numberOfAnglesToDrive = 3;
    childStates = zeros(numberOfAnglesToDrive, 3);
    cost = zeros(numberOfAnglesToDrive, 1);
    
    % store the index of the cell corresponding to each state
    childIndxs = zeros(numberOfAnglesToDrive, 1);

	% Genereate V and W vectors to be driven
	[v, w] = search.kinodynamic.generateVW(numberOfAnglesToDrive);

    poses = zeros(numberOfAnglesToDrive, 3);    
    
	for i = 1:numberOfAnglesToDrive
        [travelledDist, state, statesOverCurrentPath] = search.kinodynamic.driveAgent(currentAgentState, aGrid.resolution, v(i), w(i));
        childStates(i, :) = state;
        cost(i) = travelledDist;
        childIndxs(i) = graphs.convertXY2Indx(aGrid, state(1:2));
        % We have all of the states that the robot has passed through in
        % the current path but we just need the last one to figure out
        % where the robt has ended up!
        plot(statesOverCurrentPath(:, 1), statesOverCurrentPath(:, 2), ...
             'Color', constants.Colors.LIGHTGRAY);
        poses(i, :) = state;
	end
      
    
    weightGain = 0.05;
    x = linspace(-1, 1, numberOfAnglesToDrive);
    weight = weightGain*(x.^4 + 1) + 1;
    cost = cost.*weight';

    % Create the successor matrix and check if all neighbours are within the grid/freeSpace
    % if not don't add them to the successors matrix
%    successors(numberOfTries, 1) = 0;

    % Neighbours is a vector of potential successors. The keyword here is potential! 
    % The neighbours that do not match specific criteria should be filtered out!
    neighbours = childIndxs;
    rangeMask = neighbours > 0 & neighbours <= aGrid.nNodes;

    %% remove neighbours not in range
    neighbours = neighbours(rangeMask);
    notValid = neighbours == 0;

    successors = neighbours;
    
    if numel(successors) ~= numel(notValid)
       disp('FUCK'); 
    end

	workSpaceMask = (mod(neighbours, aGrid.dim) ~= 1) & ~(workSpace(neighbours) == 1);
    notValid = notValid | ~workSpaceMask;
    
end