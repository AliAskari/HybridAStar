function [travelledDist, previousPose, pose] = driveAgent(currentPose, res, v, w)

    t = 0.00;
    finalT = 1.5*(res)/v;
	nSteps = 100;
    
    dt = finalT/nSteps;

    previousPose = currentPose;
    
    pose = zeros(nSteps + 1, 3);
    pose(1, :) = currentPose;

    travelledDist = 0;
        
%     for i=2:nSteps
%         currentPose = search.kinodynamic.updatePose(currentPose, v, w, dt);
%         pose(i, :) = currentPose;
%         dist = norm(currentPose(1:2)-previousPose(1:2));
%         previousPose = currentPose;
%         travelledDist = travelledDist+dist;
%         t = t+dt;
%     end

    poseCounter = 0;
	while t < finalT
        poseCounter = poseCounter + 1;      
        currentPose = search.kinodynamic.updatePose(currentPose, v, w, dt);
        pose(poseCounter, :) = currentPose;
        dist = norm(currentPose(1:2)-previousPose(1:2));
        previousPose = currentPose;
        travelledDist = travelledDist+dist;
        t = t+dt;
	end

%     previousPose = currentPose;
%     
%     pose = nan(500, 3);
%     poseCounter = 1;
%     pose(poseCounter, :) = currentPose;
%     travelledDist = 0.0;
%     t = 0.0;
%     
%     % Since MATLAB does not have a do/while loop set this to a crazy large
%     % number to make the while loop run at least once. Awesome job
%     % Mathwors. Fucking unbelievable 
%     [v, w] = generateVW(direction);
% 
%     % The time that it takes for the robot to cover a whole cell
%     finalT = (1.24*res)/v;
%     dt = finalT/10;
%     
%     while t < finalT
%         poseCounter = poseCounter + 1;      
%         currentPose = updatePose(previousPose, v, w, dt);
%         pose(poseCounter, :) = currentPose;
%         dist = norm(currentPose(1:2)-previousPose(1:2));
%         previousPose = currentPose;
%         travelledDist = travelledDist+dist;
%         t = t+dt;
%     end
    
end