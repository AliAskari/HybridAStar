function newPose = updatePose(currentPose, V, W, dt)
    newPose = zeros(1, 3);
    
%     newPose(1) = currentPose(1) + V*dt*cos(currentPose(3) + (W*dt)/2);
%     newPose(2) = currentPose(2) + V*dt*sin(currentPose(3) + (W*dt)/2);
%     newPose(3) = currentPose(3) + W*dt;

%     newPose(1) = currentPose(1) + dt*V*cos(currentPose(3));
%     newPose(2) = currentPose(2) + dt*V*sin(currentPose(3));
%     newPose(3) = currentPose(3) + dt*W;

    newPose(1) = currentPose(1) + dt*V*cos(currentPose(3));
    newPose(2) = currentPose(2) + dt*V*sin(currentPose(3));
    newPose(3) = currentPose(3) + dt*W;
    
end