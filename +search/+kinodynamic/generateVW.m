function [v, w] = generateVW(numberOfTries)

    maxW = 1.4;
    w = linspace(-maxW, maxW, numberOfTries);
    v = 0.2 * ones(size(w));
    
end
