function runWrapper()

    startState = [ 03.00, 0.00, 0.00];
    goalState  = [-03.00, 0.00, 0.00];
    
    %% Add obstacles  
    % The osbtacles positions might come from the cameras
    realObstacles = false;
    
    if (realObstacles == true)
        % the first row is time. Extract it and save it for later use!
        %time = data(1, :);
        
%         % The rest of the data array is XYZTheta
%         data = data(2:end, :);
%         numberOfObstacles = (size(data, 1)) / 3;
% 
%         % I have a total of 9 valid obstacles. Load them!
%         obstacleDimensions = visual.loadObstacleDimensions();
% 
%         obstacleArray = zeros(numberOfObstacles, 5);
% 
%         % reshape the data and convert it to [n x 3] array where n is the
%         % number of obstacles
%         xyt = reshape(data(:, end), [numberOfObstacles, 3]);
        
        
        % for matlab.mat
        obstacles = obstacles.signals.values(:, :, end);
    else

        obstacles(1, :) = [-01.00,  00.00, pi/02, 02.00, 00.50];
        obstacles(2, :) = [ 00.00, -00.75, 00.00, 02.00, 00.50];
        obstacles(3, :) = [ 00.00,  00.75, 00.00, 02.00, 00.50];

    end
    
    aGrid = graphs.SearchGrid(startState, goalState);
    workSpace = graphs.createWorkSpace(aGrid);
    [allVertices, ~] = graphs.loadObstacles(aGrid, workSpace, obstacles, realObstacles);

    figure();
    mainAx = gca;       
    hold(mainAx, 'on');
    title(mainAx, 'Hybrid A* searching the workspace', 'Interpreter','LaTex')
    visual.plotContinuousWorkSpace(mainAx, aGrid, workSpace, allVertices);
    set(mainAx,'XTickLabel',{' '}, 'XTick', [])
    set(mainAx,'YTickLabel',{' '}, 'YTick', [])
    box on;
    runSearch(obstacles, startState, goalState, realObstacles);
	hold(mainAx, 'off');
    
end