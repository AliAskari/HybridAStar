classdef Colors
    properties (Constant = true)
        RED           = [213, 030, 030]./255;
        DARKRED       = [213, 000, 000]./255;
        YELLOW        = [255, 210, 000]./255;
        LIGHTBLUE     = [000, 176, 255]./255;
        DARKBLUE      = [013, 071, 161]./255;
        GREEN         = [000, 230, 118]./255;    
        LIGHTGREEN    = [000, 200, 083]./255;    
        WHITE         = [250, 250, 250]./255;
        BLACK         = [001, 001, 001]./255;
        LIGHTGRAY     = [158, 158, 158]./255;
        DARKGRAY      = [066, 066, 066]./255;
        VERYLIGHTGRAY = [238, 238, 238]./255;
        LIGHTORANGE   = [255, 109, 000]./255;
        DARKORANGE    = [191, 054, 012]./255;
        BROWN         = [141, 110, 099]./255;
        ORANGE        = [255, 152, 000]./255;
        PURPLE        = [103, 058, 183]./255;
    end    
end
