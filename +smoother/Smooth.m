function [smoothedWayPoints] = Smooth(wayPoints, numberOfWayPoints)
%#codegen
    % current number of iterations of the gradient descent smoother
    iterations = 0;
    % the maximum iterations for the gd smoother
    maxIterations = 500;

    newPath = wayPoints;

    totalWeight = smoother.CONSTANTS.SMOOTHNESSWEIGHT + smoother.CONSTANTS.CURVATUREWEIGHT;
    
    while (iterations < maxIterations)
            

        for i = 3:numberOfWayPoints-3
            
            if (isnan(newPath(i+3, 1)))
                break;
            end

            xim2 = newPath(i-2, 1:2);
            xim1 = newPath(i-1, 1:2);
            xi   = newPath(i  , 1:2);
            xip1 = newPath(i+1, 1:2);
            xip2 = newPath(i+2, 1:2);

            correction = [0, 0];

            % correction = correction - smoother.obstacleTerm(xi);
            correction = correction - smoother.smoothnessTerm(xim2, xim1, xi, xip1, xip2);
            correction = correction - smoother.curvatureTerm(xim1, xi, xip1);

            % falloff rate for the voronoi field
            alpha = 0.1;
            xi = xi + alpha * correction/totalWeight;
            newPath(i, 1) = xi(1);
            newPath(i, 2) = xi(2);

            Dxi = xi - xim1;
            newPath(i-1, 3) = atan2(Dxi(2), Dxi(1));

        end

        iterations = iterations + 1;
    end

    smoothedWayPoints = newPath;
    
end

