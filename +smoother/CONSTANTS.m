classdef CONSTANTS
    %CONSTANTS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant = true)
        
        R = 0.02;
        % maximum possible curvature of the non-holonomic vehicle
        KAPPAMAX = 1.0 / (smoother.CONSTANTS.R * 1.1);
        
        SMOOTHNESSWEIGHT = 0.2;
        CURVATUREWEIGHT  = 0.2;
        
    end
    
end

