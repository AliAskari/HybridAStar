function smoothedWayPoint = smoothnessTerm(xim2, xim1, xi, xip1, xip2)
%#codegen
	%weight for the smoothness term
  	wSmoothness = 0.2;
	smoothedWayPoint = wSmoothness * (xim2 - 4 * xim1 + 6 * xi - 4 * xip1 + xip2);
    
end

