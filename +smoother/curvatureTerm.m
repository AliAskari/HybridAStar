function gradient = curvatureTerm(xim1, xi, xip1)
%#codegen

    gradient = zeros(1, 2);
    % the vectors between the nodes
    Dxi = xi - xim1;
    Dxip1 = xip1 - xi;
    % orthogonal complements vector
    p1 = zeros(1, 2);
    p2 = zeros(1, 2);

    % the distance of the vectors
    absDxi = norm(Dxi);
    absDxip1 = norm(Dxip1);

    % ensure that the absolute values are not null
    if (absDxi > 0 && absDxip1 > 0) 

        temp = helper.clamp(dot(Dxi, Dxip1) / (absDxi * absDxip1), -1, 1);
        Dphi = acos(temp);
        kappa = Dphi / absDxi;

        % if the curvature is smaller then the maximum do nothing
        if (kappa <= smoother.CONSTANTS.KAPPAMAX) 
            gradient = zeros(1, 2);
            return;
        else
            absDxi1Inv = 1 / absDxi;
            PDphi_PcosDphi = -1 / sqrt(1 - cos(Dphi)^2);
            u = -absDxi1Inv * PDphi_PcosDphi;
            % calculate the p1 and p2 terms
            p1 =  helper.ort(xi  , -xip1) / (absDxi * absDxip1);
            p2 = -helper.ort(xip1, xi)    / (absDxi * absDxip1);
            % calculate the last terms
            s = Dphi / (absDxi * absDxi);
            
            ki = u * (-p1 - p2) - (s * ones(1, 2));
            kim1 = u * p2 - (s * ones(1, 2));
            kip1 = u * p1;            

            wCurvature = 0.001;
            % calculate the gradient
            gradient = wCurvature * (0.25 * kim1 + 0.5 * ki + 0.25 * kip1);

            if isnan(gradient(1)) || isnan(gradient(2))
                gradient = zeros(1, 2);
                return;
            else
                return;
            end
        end

    else
        gradient = zeros(1, 2);
        return;
    end


end

