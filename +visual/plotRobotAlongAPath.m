function plotRobotAlongAPath(ax, robotState)

    currentRobotState =robotState;
    
    qgvVertices = [-0.0929   -0.1800;
                    0.2090   -0.1800;
                    0.3112   -0.0755;
                    0.3112    0.0755;
                    0.2090    0.1800;
                   -0.0929    0.1800];

    qgvVertices = -0.4*qgvVertices;

    robotHeading = currentRobotState(3);
    transformationMatrix = [cos(robotHeading),  sin(robotHeading);
                            -sin(robotHeading), cos(robotHeading)];

    transformedVertices = qgvVertices*transformationMatrix;
    xyVerticesGlobal = bsxfun(@plus, transformedVertices, currentRobotState(1:2));               

    % plot robot 
    robotColor = constants.Colors.GREEN;
    visual.plotPolygon(ax, xyVerticesGlobal, robotColor, 1, 1, 0.7, false);

    % Plot robot mass point
%     plot(ax, currentRobotState(1), currentRobotState(2), ...
%              'Color', constants.Colors.RED, ...
%              'Marker', '*', ...
%              'MarkerSize', 12);
      
end

