function plotHandle = plotPath(grid, workSpace, path)
    
    plotHandle = 0;
    
    % just plot the path; leave the start and goal nodes alone!
    for i = 2:size(path, 1) - 1
        if plotHandle == 0
            [workSpace, plotHandle] = visual.plotNode(path(i), grid, workSpace);
        else
            [workSpace, plotHandle] = visual.plotNode(path(i), grid, workSpace, plotHandle);
        end
    end
end

