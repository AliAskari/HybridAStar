function h = plotContinuousWorkSpace(ax, aGrid, workSpace, allVertices)

    gridOffSet = 0.4;
    axis(ax, [graphs.SearchGrid.LIMITS_X(1) - gridOffSet, graphs.SearchGrid.LIMITS_X(2) + gridOffSet, ...
              graphs.SearchGrid.LIMITS_Y(1) - gridOffSet, graphs.SearchGrid.LIMITS_Y(2) + gridOffSet]); 

    plot(ax, aGrid.startState(1), aGrid.startState(2), ...
        'Marker', 'o', ...
        'Color',  constants.Colors.DARKBLUE, ...
        'MarkerFaceColor', constants.Colors.LIGHTBLUE, ...
        'MarkerSize', 7);

    plot(ax, aGrid.goalState(1), aGrid.goalState(2), ...
        'Marker', 'o', ...                        
        'Color', constants.Colors.DARKORANGE, ...
        'MarkerFaceColor', constants.Colors.LIGHTORANGE, ...
        'MarkerSize', 7);

    set(ax, 'Color', constants.Colors.VERYLIGHTGRAY);


    visual.plotObstacles(allVertices);


    
end