function [workSpace, h] = plotNode(node, aGrid, workSpace, varargin)
    
    switch nargin
        case 4
            % Use the passed handle to modify the plot
            h = varargin{1};
        case 3
            % No handle to the figure is passed, so create one!
            h = visual.plotWorkSpace(aGrid, workSpace);
        otherwise
            error('Number of arguments does not match function signature.');
    end
    
    % Check if the node is passed as index or [x, y]
    if numel(node) == 2
        nodeIndx = graph.convertXY2Indx(aGrid, node);
    elseif numel(node) == 1
        nodeIndx = node;
    end
    
    
    % then update the workSpace value correponding to this index
    workSpace(nodeIndx) = 5;    
    set(h, 'CData', workSpace);    
       
    cmap = [Colors.WHITE;          % freeSpace node  (white)  workSpace = 0
            Colors.BLACK;          % wallSpace node  (black)  workSpace = 1
            Colors.RED;            % startState node (red)    workSpace = 2
            Colors.BLUE;           % goalState node  (blue)   workSpace = 3
            Colors.GREEN;          % visited node    (green)  workSpace = 4
            Colors.YELLOW];        % path nodes      (yellow) workSpace = 5 
        
    axis square
    colormap(cmap); 

end

