function h = plotWorkSpace(aGrid, workSpace)

%     figure();
    % To make sure the start node and goal node are not covered by a
    % different color we always override their values
    workSpace(aGrid.goalNodeIndx)  = 2;
    workSpace(aGrid.startNodeIndx) = 3;

    h = pcolor(aGrid.gridX, aGrid.gridY, workSpace);
    
    % if grid resolution is too small it won't look good, so it makes sense
    % to get rid of the grid in such cases
    if aGrid.resolution < 0.05
        shading flat;
    end

    howManyColorToShow = max(workSpace(:));
    
    if howManyColorToShow >= 4
        cmap = [constants.Colors.WHITE;          % freeSpace       (white)  workSpace = 0
                constants.Colors.BLACK;          % wallSpace       (black)  workSpace = 1
                constants.Colors.RED;            % startState node (red)    workSpace = 2
                constants.Colors.YELLOW;         % goalState node  (blue)   workSpace = 3
                constants.Colors.DARKBLUE;       % path nodes      (yellow) workSpace = 5
                constants.Colors.GREEN];         % visited nodes   (green)  workSpace = 4
                        
    elseif howManyColorToShow >=3
        cmap = [constants.Colors.WHITE;          % freeSpace       (white)  workSpace = 0
                constants.Colors.BLACK;          % wallSpace       (black)  workSpace = 1
                constants.Colors.RED;            % startState node (red)    workSpace = 2
                constants.Colors.DARKBLUE];      % goalState node  (blue)   workSpace = 3
    end
    
    % Set axis units to be the same on both axes and limit it to the
    % current workspace size
    box on;
    axis equal tight    
    % axis([grid.limitsX(1), grid.limitsX(2), grid.limitsY(1), grid.limitsY(2)]);       
    colormap(cmap); 

    
end