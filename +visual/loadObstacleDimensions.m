function obstacleDimensions = loadObstacleDimensions()

    obstacleDimensions = zeros(4, 2);

    WIDTH = 0.100;
    
	obstacleDimensions(1, 1) = 1.25;
    obstacleDimensions(1, 2) = WIDTH;
	obstacleDimensions(2, 1) = 1.25;
    obstacleDimensions(2, 2) = WIDTH;
    obstacleDimensions(3, 1) = 1.25;
    obstacleDimensions(3, 2) = WIDTH;
    obstacleDimensions(4, 1) = 0.665;
    obstacleDimensions(4, 2) = WIDTH;    
    
%     obstacleDimensions(1, 1) = 1.215;
%     obstacleDimensions(1, 2) = WIDTH;
% 	obstacleDimensions(2, 1) = 1.215; 
%     obstacleDimensions(2, 2) = WIDTH;
% 	obstacleDimensions(3, 1) = 1.190;
%     obstacleDimensions(3, 2) = WIDTH;
% 	obstacleDimensions(4, 1) = 1.200;
%     obstacleDimensions(4, 2) = WIDTH;
% 	obstacleDimensions(5, 1) = 1.200;
%     obstacleDimensions(5, 2) = WIDTH;
% 	obstacleDimensions(6, 1) = 1.200;
%     obstacleDimensions(6, 2) = WIDTH;
% 	obstacleDimensions(7, 1) = 1.190;
%     obstacleDimensions(7, 2) = WIDTH;
% 	obstacleDimensions(8, 1) = 0.665;
%     obstacleDimensions(8, 2) = WIDTH;
%     obstacleDimensions(9, 1) = 0.210;
%     obstacleDimensions(9, 2) = WIDTH;
    
end