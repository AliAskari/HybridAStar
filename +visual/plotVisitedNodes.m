function plotVisitedNodes(grid, visitedNodes)

    workspace = reshape(visitedNodes, size(grid.gridX, 1), size(grid.gridX, 1));
    h = pcolor(grid.gridX, grid.gridY, workspace);
    set(h, 'EdgeColor', 'none');
    
    cmap = [1, 1, 1;	%freeSpace (white) workSpace = 0
            0, 0, 0;	%wallSpace (black) workSpace = 1
            1, 0, 0; 	%startState(red)   workSpace = 2
            0, 0, 1]; 	%goalState (blue)  workSpace = 3
        
    axis square
    colormap(cmap); 

end