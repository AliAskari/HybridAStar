function plotPolygon(ax, vertices, color, showEdge, width, transparency, faceColor)
%#codegen

    %Declare patch as extrinsic
    coder.extrinsic('patch'); 
    
    if nargin == 1
        color = 'blue';
    end
    
    x = vertices(:, 1);
    y = vertices(:, 2);
    
    if showEdge == 0
        
        if faceColor == true
            patch(x, y, -4*ones(size(x)), color, 'Parent', ax, ...
                  'EdgeColor','None', 'LineWidth', width);
        else
            patch(x, y, -4*ones(size(x)), color, 'Parent', ax, ...
                  'EdgeColor','None', 'LineWidth', width, ...
                  'FaceColor','none');
        end
        
    else
        
        if faceColor == true
            patch(x, y, -4*ones(size(x)), color, 'Parent', ax, ...
                  'EdgeColor', 'Black', 'LineWidth', width, ...
                  'FaceAlpha', transparency);
        else
            patch(x, y, -4*ones(size(x)), color, 'Parent', ax, ...
                  'EdgeColor', 'Black', 'LineWidth', width, ...
                  'FaceColor','none', 'FaceAlpha', transparency);
        end

    end
    
end