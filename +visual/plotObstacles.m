function plotObstacles(obstacleVertices)
%#codegen

	% Draw obstacles
    for obstacleCounter = 1:size(obstacleVertices, 1)
        visual.plotPolygon(gca, obstacleVertices{obstacleCounter}, constants.Colors.DARKGRAY, 1, 1, 1, true);
    end
    
end

