function plotRobotPath(robotStates, varargin)
      
    % Check if we have to draw to obstacles
    nVarargs = length(varargin);        
    drawObstacles = false;
    if nVarargs > 0
        obstacleVertices = varargin{1};
        drawObstacles = true;
    end
        
    
    hold on
    %% Find number of states to print!
    %nStates = 0;
    for i = 1:size(robotStates, 1)
        if isnan(robotStates(i, 1))
            break;
        else
            %nStates = nStates + 1;
            currentRobotState = robotStates(i, :);
            qgvVertices = [-0.0929   -0.1800;
                            0.2090   -0.1800;
                            0.3112   -0.0755;
                            0.3112    0.0755;
                            0.2090    0.1800;
                           -0.0929    0.1800];
    
            qgvVertices = -0.4*qgvVertices;

            robotHeading = currentRobotState(3);
            transformationMatrix = [cos(robotHeading),  sin(robotHeading);
                                    -sin(robotHeading), cos(robotHeading)];

            transformedVertices = qgvVertices*transformationMatrix;
            xyVerticesGlobal = bsxfun(@plus, transformedVertices, currentRobotState(1:2));               

            % plot robot 
            ax = gca;  
            robotColor = constants.Colors.GREEN;
            visual.plotPolygon(ax, xyVerticesGlobal, robotColor, 1, 1, 0.7, false);
            
            % Plot robot mass point
            plot(currentRobotState(1), currentRobotState(2), ...
                'Color', constants.Colors.RED, 'Marker', '.', ...
                'MarkerSize', 12);
            
            
            ax = gca;
            if drawObstacles == true
                % Draw obstacles
                for obstacleCounter = 1:size(obstacleVertices, 1)
                    obstacleColor = [100, 100, 100]./255;
                    visual.plotPolygon(ax, obstacleVertices{obstacleCounter}, obstacleColor, 1, 1, 1, true);
                end
            end
        end
    end
    hold off
    gridOffSet = 0.2;
    axis([graphs.SearchGrid.LIMITS_X(1) - gridOffSet, graphs.SearchGrid.LIMITS_X(2) + gridOffSet, ...
          graphs.SearchGrid.LIMITS_Y(1) - gridOffSet, graphs.SearchGrid.LIMITS_Y(2) + gridOffSet]);       
    
end

