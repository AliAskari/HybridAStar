function [goalState, stopThreshold] = Init()
  
    % Initialize
    goalState     = [-1.00, 1.6, 0];
    stopThreshold = 0.15;

end