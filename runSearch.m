function [smoothedWayPoints, allVertices] = runSearch(obstacles, startState, goalState, RealObsatcles)
%#codegen
    
%     close all;
    %% Initialize
%     startState = [QGV(2, currentIteration), QGV(3, currentIteration), QGV(4, currentIteration)];
    
    
    %% Create grid and workspace which is used for search
    aGrid = graphs.SearchGrid(startState, goalState);
    workSpace = graphs.createWorkSpace(aGrid);
    [allVertices, workSpace] = graphs.loadObstacles(aGrid, workSpace, obstacles, RealObsatcles);
%     visual.plotWorkSpace(aGrid, workSpace);

    
    %% Prepare for aearch
%     aGrid.heuristic = search.calculateHeuristic(aGrid, 'euclidean');
%     [repulsivePotential, ~] = potential.calculatePotentialFunction(aGrid, allVertices);
%     aGrid.cost = repulsivePotential;
%     figure();
%     pcolor(aGrid.gridX, aGrid.gridY, reshape(aGrid.cost, size(workSpace)));
    wayPoints = nan([constants.Grid.MAX_NUMBER_OF_WAYPOINTS, 3]);
    
    %% Do the fucking Search - A*
    [~, visitedNodes, cameFrom, searchAgentState, dubinsNodes, dubinsCollision, numberOfDubinsNodes, lastExpandedNode] = search.aStarHybrid(aGrid, workSpace, allVertices);
    if (isnan(lastExpandedNode))
        smoothedWayPoints = wayPoints;
        disp('Failed to find the goal.');
        return; 
    end
    
%     figure();
%     pcolor(aGrid.gridX, aGrid.gridY, reshape(costSoFar, size(workSpace)));
%     figure();
    workSpace(visitedNodes == 1) = 4;
    
    %% Find the path using the cameFrom vector
	[pathOfStates, numberOfWayPoints] = search.findPath(aGrid, cameFrom, searchAgentState, lastExpandedNode);
        
    for i = 1:numberOfWayPoints
        % pathOfStates contains the waypoints in reverse order!
        if (pathOfStates(i) ~= inf)
            wayPoints(i, :) = pathOfStates((numberOfWayPoints+1) - i, :);
        end
    end
    
    % If there were no dubins collision add the nodes
    if (dubinsCollision == false)
        % Now add the nodes from Dubins path!
        for n = 1:3:numberOfDubinsNodes
            numberOfWayPoints = numberOfWayPoints + 1;
            wayPoints(numberOfWayPoints, :) = dubinsNodes(n, :);
        end
        numberOfWayPoints = numberOfWayPoints + 1;
        wayPoints(numberOfWayPoints, :) = dubinsNodes(numberOfDubinsNodes, :);
    end
    
   	% iterate over wayPoints and check if they are valid!
    for k = 1:numberOfWayPoints
        nodeIndx = graphs.convertXY2Indx(aGrid, wayPoints(k, 1:2));
        if (nodeIndx > 0)
            workSpace(nodeIndx) = 5;
        else
            disp('FUCK');
        end
    end
    
    smoothedWayPoints = smoother.Smooth(wayPoints, numberOfWayPoints);
    
end