function [status, outputs] = GetDubinsWords(dubinPathType, alpha, beta, d, outputs)

    sa   = sin(alpha);           
    sb   = sin(beta);            
    ca   = cos(alpha);           
    cb   = cos(beta);            
    c_ab = cos(alpha - beta); 

    if dubinPathType == dubins.CONSTANTS.LSL

        tmp0 = d+sa-sb;
        p_squared = 2 + (d*d) -(2*c_ab) + (2*d*(sa - sb));

        if p_squared < 0
            status = dubins.CONSTANTS.ERRNOPATH;
            return;
        end

        tmp1 = atan2( (cb-ca), tmp0 );
        t    = helper.mod2pi(-alpha + tmp1 );
        p    = sqrt( p_squared );
        q    = helper.mod2pi(beta - tmp1 );

        outputs(1)  = t;
        outputs(2)  = p;
        outputs(3)  = q;
        
                status = dubins.CONSTANTS.ERROK;
        return;

    elseif dubinPathType == dubins.CONSTANTS.LSR

        p_squared = -2 + (d*d) + (2*c_ab) + (2*d*(sa+sb));

        if p_squared < 0 
            status = dubins.CONSTANTS.ERRNOPATH;
            return;
        end

        p      = sqrt( p_squared );
        tmpLSR = atan2( (-ca-cb), (d+sa+sb) ) - atan2(-2.0, p);
        t      = helper.mod2pi(-alpha + tmpLSR);
        q      = helper.mod2pi( -helper.mod2pi(beta) + tmpLSR);
        outputs(1)  = t;                
        outputs(2)  = p;                
        outputs(3)  = q;

        status = dubins.CONSTANTS.ERROK;
        return;

    elseif dubinPathType == dubins.CONSTANTS.RSL

        p_squared = (d*d) -2 + (2*c_ab) - (2*d*(sa+sb));

        if p_squared < 0
            status = dubins.CONSTANTS.ERRNOPATH;
            return;
        end

        p      = sqrt(p_squared);
        tmpRSL = atan2((ca+cb), (d-sa-sb)) - atan2(2.0, p);
        t      = helper.mod2pi(alpha - tmpRSL);
        q      = helper.mod2pi(beta - tmpRSL);

        outputs(1)  = t;                
        outputs(2)  = p;                
        outputs(3)  = q;

        status = dubins.CONSTANTS.ERROK;
        return;

    elseif dubinPathType == dubins.CONSTANTS.RSR

        tmp0 = d-sa+sb;
        p_squared = 2 + (d*d) -(2*c_ab) + (2*d*(sb-sa));

        if p_squared < 0
            status = dubins.CONSTANTS.ERRNOPATH;
            return;
        end

        tmpRSR = atan2((ca-cb), tmp0);
        t      = helper.mod2pi(alpha - tmpRSR);
        p      = sqrt(p_squared);
        q      = helper.mod2pi(-beta + tmpRSR);
        
        outputs(1)  = t;                
        outputs(2)  = p;                
        outputs(3)  = q;

        status = dubins.CONSTANTS.ERROK;
        return;


    elseif dubinPathType == dubins.CONSTANTS.RLR
    
        tmpRLR = (6.0 - d*d + 2*c_ab + 2*d*(- sa + sb)) / 8.0;

        if abs(tmpRLR) > 1
            status = dubins.CONSTANTS.ERRNOPATH;
            return;
        end

        p = helper.mod2pi(2*pi - acos(tmpRLR));
        t = helper.mod2pi(-alpha - atan2( ca-cb, d+sa-sb) + p/2);
        q = helper.mod2pi(helper.mod2pi(beta) - alpha -t + helper.mod2pi(p));
        
        outputs(1)  = t;                
        outputs(2)  = p;                
        outputs(3)  = q;

        status = dubins.CONSTANTS.ERROK;
        return;


    elseif dubinPathType == dubins.CONSTANTS.LRL

        tmpLRL = (6.0 - d*d + 2*c_ab + 2*d*(sa-sb)) / 8.0;

        if abs(tmpLRL) > 1
            status = dubins.CONSTANTS.ERRNOPATH;
            return;
        end

        p = helper.mod2pi(2*pi - acos(tmpLRL));
        t = helper.mod2pi(alpha - atan2(ca-cb, d-sa+sb) + helper.mod2pi(p/2.));
        q = helper.mod2pi(alpha - beta - t + helper.mod2pi(p));

        outputs(1)  = t;                
        outputs(2)  = p;                
        outputs(3)  = q;

        status = dubins.CONSTANTS.ERROK;
        return;

    else
        status = dubins.CONSTANTS.ERRNOPATH;
        return;
    end

end


