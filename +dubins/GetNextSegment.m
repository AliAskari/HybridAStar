function qt = GetNextSegment(t, qi, pathType)

    qt = nan(1, 3);
    if pathType == dubins.CONSTANTS.L_SEG
        
        qt(1) = qi(1) + sin(qi(3)+t) - sin(qi(3));
        qt(2) = qi(2) - cos(qi(3)+t) + cos(qi(3));
        qt(3) = qi(3) + t;

    elseif pathType == dubins.CONSTANTS.R_SEG
        
        qt(1) = qi(1) - sin(qi(3)-t) + sin(qi(3));
        qt(2) = qi(2) + cos(qi(3)-t) - cos(qi(3));
        qt(3) = qi(3) - t;

    elseif pathType == dubins.CONSTANTS.S_SEG

        qt(1) = qi(1) + cos(qi(3)) * t;
        qt(2) = qi(2) + sin(qi(3)) * t;
        qt(3) = qi(3);

    end
    
end