function [dubinsNodes, numberOfNodes, pathLength, collision] = Shoot(workSpace, aGrid, startState, goalState, verticesList, minTurnRadius)

    aDubinsPath = dubins.DubinsPath();
    aDubinsPath = dubins.Init(startState, goalState, minTurnRadius, aDubinsPath);
    
    pathLength = aDubinsPath.GetPathLength();
    
    dubinsNodes = nan(dubins.CONSTANTS.MAXNODESINPATH, 3);
    
    travelledDistance = 0;
    stateAtThisNode = [nan, nan, nan];
    
    numberOfNodes = 0;
    collision = false;
    
    for i = 1:dubins.CONSTANTS.MAXNODESINPATH

        if travelledDistance > pathLength
            break;
        end
        
        
        [errorCode, stateAtThisNode] = dubins.PathSample(aDubinsPath, travelledDistance);
        if (errorCode ~= dubins.CONSTANTS.ERROK)
            error('You are fucked!');
        end
        
        dubinsNodes(i, :) = stateAtThisNode(1, :);

        % collision check
        % if (configurationSpace.isTraversable(dubinsNodes(i)))
        nodeStatus = dubins.IsNodeTraversable(aGrid, workSpace, dubinsNodes(i, :));
        if nodeStatus == true
          travelledDistance = travelledDistance + dubins.CONSTANTS.STEPSIZE;
        else
          % There is a collision. You are screwed.
          collision = true;
          return;
        end
        
%         % Then find the distance of dubin node to obstacles. If we are too
%         % close, it's not a valid node!
%         dubinsNodeIndx = graphs.convertXY2Indx(aGrid, dubinsNodes(i, :));
%         isValid = potential.calculatePotentialAtThisNode(dubinsNodeIndx, aGrid, verticesList);
%         
%         if (isValid ~= 0)
%             %There is a collision. You are screwed.
%             collision = true;
%             return;
%         end
        
        
        numberOfNodes = numberOfNodes + 1;

    end
    
end

