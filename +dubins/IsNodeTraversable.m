function nodeStatus = IsNodeTraversable(aGrid, workSpace, CurrentNode)

    currentIndex = graphs.convertXY2Indx(aGrid, CurrentNode(1:2));

    if (currentIndex < 0)
        % It's on an occupied cell
        nodeStatus = false;   
        return;
    end
    
    % If the node is the starting point or goal point
    if workSpace(currentIndex) == 0 || ...
       workSpace(currentIndex) == 2 || ...
       workSpace(currentIndex) == 3
        % It's free
        nodeStatus = true;
    else
        % It's on an occupied cell
        nodeStatus = false;        
    end

end