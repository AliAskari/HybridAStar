function aDubinsPath = Init(startState, goalState, minTurnRadius, aDubinsPath)
    
    % It doesn't make sense to have a negative or 0 turning radius
    if (minTurnRadius <= 0)
    	error('The turning radius must be a positive non-zero number.')
    end

    deltaX = goalState(1) - startState(1);
    deltaY = goalState(2) - startState(2);

    dist2Goal = sqrt(deltaX^2 + deltaY^2);
    d = dist2Goal/minTurnRadius;

    theta = atan2(deltaY, deltaX);
    alpha = startState(3) - theta;
    beta  = goalState(3) - theta;

    for i = 1:3
    	aDubinsPath.startState(i) = startState(i);
    end
    
    aDubinsPath.minTurnRadius = minTurnRadius;

    aDubinsPath = dubins.Normalize(alpha, beta, d, aDubinsPath);


end


