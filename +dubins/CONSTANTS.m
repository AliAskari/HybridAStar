classdef CONSTANTS

    properties (Constant = true)
        
        % This is the minimum distance to the goal that triggers the dubins
        % path shoot call!
        TRIGGERDISTANCE = 2.0;
        
        % This should be at least half of the grid resolution
        STEPSIZE = 0.025;

        % The epsilon along dubins path. This is way too small
        EPSILON = 10e-10;

        % All 6 different type of a dubin path
        LSL = 1;
        LSR = 2;
        RSL = 3;
        RSR = 4;
        RLR = 5;
        LRL = 6;

        % The three general path types
        L_SEG = 0;
        S_SEG = 1;
        R_SEG = 2;

        % The segment types for each of the path types
        DUBINDIRDATA = [dubins.CONSTANTS.L_SEG, dubins.CONSTANTS.S_SEG, dubins.CONSTANTS.L_SEG; ...
                        dubins.CONSTANTS.L_SEG, dubins.CONSTANTS.S_SEG, dubins.CONSTANTS.R_SEG; ... 
                        dubins.CONSTANTS.R_SEG, dubins.CONSTANTS.S_SEG, dubins.CONSTANTS.L_SEG; ... 
                        dubins.CONSTANTS.R_SEG, dubins.CONSTANTS.S_SEG, dubins.CONSTANTS.R_SEG; ... 
                        dubins.CONSTANTS.R_SEG, dubins.CONSTANTS.L_SEG, dubins.CONSTANTS.R_SEG; ... 
                        dubins.CONSTANTS.L_SEG, dubins.CONSTANTS.R_SEG, dubins.CONSTANTS.L_SEG];

        % Error codes
        ERROK         = 0;   % No error
        ERRCOCONFIGS  = 1;   % Colocated configurations
        ERRPARAM      = 2;   % Path parameterisitation error
        ERRBADRHO     = 3;   % the radius is invalid
        ERRNOPATH     = 4;   % No connection between configurations with this word
        
        %
        MAXNODESINPATH = 1500;

    end

end