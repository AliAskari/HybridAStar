function aDubinsPath = Normalize(alpha, beta, d, aDubinsPath)

    bestCost = inf;
    bestWord = -1;

    for i = 1:6
        
        params = nan(1, 3);
        [err, output] = dubins.GetDubinsWords(i, alpha, beta, d, params);

        if err == dubins.CONSTANTS.ERROK
            
            cost = output(1) + output(2) + output(3);            
            if cost < bestCost
                bestWord = i;
                bestCost = cost;
                aDubinsPath.segLengths(1) = output(1);
                aDubinsPath.segLengths(2) = output(2);
                aDubinsPath.segLengths(3) = output(3);
                aDubinsPath.pathType      = i;
            end

        end

    end
    
end

