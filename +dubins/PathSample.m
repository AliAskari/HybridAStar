function [errorCode, currentState] = PathSample(aDubinsPath, t)

    errorCode = dubins.CONSTANTS.ERROK;
    currentState = [nan, nan, nan];
    
    if (t < 0) || (t >= aDubinsPath.GetPathLength()) 
        % error, parameter out of bounds
        errorCode = dubins.CONSTANTS.ERRPARAM;
        return;
    end

    % tprime is the normalised variant of the parameter t
    tprime = t / aDubinsPath.minTurnRadius;

    % In order to take rho != 1 into account this function needs to be more complex
    % than it would be otherwise. The transformation is done in five stages.
    %
    % 1. translate the components of the initial configuration to the origin
    % 2. generate the target configuration
    % 3. transform the target configuration
    %      scale the target configuration
    %      translate the target configration back to the original starting point
    %      normalise the target configurations angular component

    % The translated initial configuration
    qi = [0, 0, aDubinsPath.startState(3)];

    % Generate the target configuration
    types = dubins.CONSTANTS.DUBINDIRDATA(aDubinsPath.pathType, :);

    p1 = aDubinsPath.segLengths(1);
    p2 = aDubinsPath.segLengths(2);

    q1 = [nan, nan, nan]; % end-of segment 1
    q2 = [nan, nan, nan]; % end-of segment 2

    temp = dubins.GetNextSegment(p1, qi, types(1));
    q1 = temp;

    temp = dubins.GetNextSegment(p2, q1, types(2));
    q2 = temp;

    if tprime < p1
        currentState =dubins.GetNextSegment(tprime, qi, types(1));
    elseif tprime < (p1+p2) 
        currentState = dubins.GetNextSegment( tprime-p1, q1, types(2));
    else
        currentState = dubins.GetNextSegment(tprime-p1-p2, q2, types(3));
    end

    % scale the target configuration, translate back to the original starting point
    currentState(1) = currentState(1) * aDubinsPath.minTurnRadius + aDubinsPath.startState(1);
    currentState(2) = currentState(2) * aDubinsPath.minTurnRadius + aDubinsPath.startState(2);
    currentState(3) = helper.mod2pi(currentState(3));
    
end