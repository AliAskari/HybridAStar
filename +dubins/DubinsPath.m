classdef DubinsPath

    properties
        startState;
        segLengths;
        minTurnRadius;
        pathType;
    end
    
    methods
    
        function obj = DubinsPath()
            obj.startState    = [nan, nan, nan];
            obj.segLengths    = [nan, nan, nan];
            obj.minTurnRadius = nan;
            obj.pathType      = nan;
        end

        function length = GetPathLength(obj)

           length = obj.segLengths(1);
           length = length + obj.segLengths(2);
           length = length + obj.segLengths(3);
           length = length * obj.minTurnRadius;

        end        
        
    end
    
end
