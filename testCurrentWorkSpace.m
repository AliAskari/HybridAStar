function paths = testCurrentWorkSpace()

	load('obstaclesPose.mat');
    load('realPose.mat');    
    % Check number of times the obstacle were moved and save the iteration
    % index
    [~, searchIndx] = find(QGV(14, :));    
    numberOfSearches = numel(searchIndx);
    
    searchIndx = 828;
    
    paths = cell(10, 1);
    
    globalIterCounter = 9;
    
    for counter = 1:numel(searchIndx)

         
        currentIteration = searchIndx(counter);
        % Ignore the first 10 iterations
%         if (currentIteration ~= 828)
%             continue;
%         end
        
        if currentIteration == searchIndx(end)
            endIteration = size(QGV, 2);
        else
            endIteration = searchIndx(counter+1);
        end
        
        X = QGV(2, currentIteration-2:endIteration);
        Y = QGV(3, currentIteration-2:endIteration);
        
        globalIterCounter = globalIterCounter + 1;
        % Ignore the first 10 iterations
        % currentIteration == 9;
        % currentIteration == 324;
        % currentIteration == 711;
        % currentIteration == 959
%         if (currentIteration == 1041)
%             almightyIteration = true;
%         end
% 
%         if (almightyIteration == false)
%             continue;
%         end
        
        xytObstacles = OBSTACLES(2:end, currentIteration);

        % Get number of obstacles and reorder them!
        numberOfObstacles = numel(xytObstacles)/3;
        % The order of the input is:
        % [x1, x2, ..., xn, y1, y2, ..., yn, t1, t2, ..., tn]
        % Reshape it to this form: [x1, y1, t1, x2, y2, t2, ..., xn, yn, tn]
        obstacles = reshape(xytObstacles(:), [numberOfObstacles, 3]);

        
        startState = [QGV(2, currentIteration), QGV(3, currentIteration), QGV(4, currentIteration)];
        [goalState, ~] = Init();
        aGrid = graphs.SearchGrid(startState, goalState);
        workSpace = graphs.createWorkSpace(aGrid);
        [allVertices, ~] = graphs.loadObstacles(aGrid, workSpace, obstacles);
 
        figure();
        mainAx = gca;       
        hold(mainAx, 'on');
        title(mainAx, 'Hybrid A* searching the workspace', 'Interpreter','LaTex')
        obstacles(2, 2) = -0.25;
        [allVertices, ~] = graphs.loadObstacles(aGrid, workSpace, obstacles);
        visual.plotContinuousWorkSpace(mainAx, aGrid, workSpace, allVertices);
        set(mainAx,'XTickLabel',{' '}, 'XTick', [])
        set(mainAx,'YTickLabel',{' '}, 'YTick', [])
        box on;
        
        obstacles(2, 2) = -0.4247;
        %% Run the search
        tic        
        [smootherWayPoints, ~] = runSearch(obstacles, startState, goalState, globalIterCounter, mainAx);
        totalTime = toc;
        fprintf('Total Time: %f seconds\n', totalTime);

%         %% plot the path that the robot should follow
%         smootherWayPoints = smootherWayPoints(1:2:end, :);
%         currentPath = smootherWayPoints;
%         plot(mainAx, X, Y, 'r*');
%         visual.plotRobotPath(smootherWayPoints);        
        
        hold(mainAx, 'off');
        

        paths{counter} = smootherWayPoints;
        %% plot the workspace
%         visual.plotWorkSpace(aGrid, workSpace);
%         pause;

        close all;
        
    end


end