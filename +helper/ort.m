function c = ort(a, b)

  % multiply b by the dot product of this and b then divide it by b's length
  c = a - b * dot(a, b) / (b(1)^2 + b(2)^2);
  
end