function clamped = clamp(n, lower, upper) 

	clamped = max(lower, min(n, upper));
	
end